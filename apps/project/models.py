#coding=utf-8
from uliweb.orm import *

class RepoType(Model):
    """
    仓库类型
    """
    repotype_name = Field(str, verbose_name='仓库类型名称', max_length=10, required=True, unique=True, index=True )

class Repo(Model):
    """
    仓库配置
    """
    repo_name = Field(str, verbose_name='仓库名称', max_length=50, required=True, unique=True, index=True )
    repo_username = Field(str, verbose_name='仓库用户名', max_length=100)
    repo_passwd = Field(str, verbose_name='仓库密码', max_length=128)
    repo_url = Field(str, verbose_name='仓库地址', max_length=200)
    repotype_id = Reference(RepoType, verbose_name='关联仓库类型ID')

class Rsync(Model):
    """
    rsync同步
    """
    rsync_module = Field(str, verbose_name='rsync同步模块名', max_length=100, required=True, unique=True, index=True )
    svn_webdir_server = Field(str, verbose_name='服务端svn web目录' , max_length=200)
    svn_webdir_client = Field(str, verbose_name='客户端端svn web目录' , max_length=200)
    svn_id = Reference(Repo, collection_name='svn_id')
    svn_msg = Field(str, verbose_name='执行svn后返回的消息' , max_length=200)
    svn_hook = Field(str, verbose_name='用于svn的钩子' , max_length=200)
