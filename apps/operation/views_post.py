#coding=utf-8
from uliweb import expose

#可以省略直接引用
from uliweb import request, error, redirect

from sqlalchemy import or_

def __begin__():
    """
    用户验证
    """
    from uliweb import functions
    return functions.require_login()

@expose('/operationpost')
class PostData(object):

    def __init__(self):
        from uliweb.orm import get_model
        self.departmentmodel = get_model('department')
        self.opersgroupmodel = get_model('opersgroup')
        self.opersgroupprojectmodel = get_model('opersgroupproject')
        self.applicationtypemodel = get_model('applicationtype')

    def department(self):
        """
        返回给jquery easyui的datagride

        所有部门的数据
        """
        pageIndex= int(request.POST.get('page',0))
        pageSize = int(request.POST.get('rows',20))

        if pageIndex == 0:
            pageIndex = 1
            content = 0
        else:
            content = pageSize * (pageIndex - 1)

        data = {}
        data['total'] = self.departmentmodel.all().count()
        data['rows'] = []


        data_info = {'data':self.departmentmodel.all().order_by(self.departmentmodel.c.id).limit(pageSize).offset(content)}

        for i in data_info['data']:

            data['rows'].append({"id":i.id,
                                 "department_name":i.department_name,
                                 "comment":i.comment
                                 })
        #返回json串
        return json(data)  

    
    def opersgroup(self):
        """
        返回给jquery easyui的datagride

        所有运营组
        """
        pageIndex= int(request.POST.get('page',0))
        pageSize = int(request.POST.get('rows',20))

        if pageIndex == 0:
            pageIndex = 1
            content = 0
        else:
            content = pageSize * (pageIndex - 1)

        data = {}
        data['total'] = self.opersgroupmodel.all().count()
        data['rows'] = []


        data_info = {'data':self.opersgroupmodel.all().order_by(self.opersgroupmodel.c.id).limit(pageSize).offset(content)}

        for i in data_info['data']:

            data['rows'].append({"id":i.id,
                                 "opersgroup_name":i.opersgroup_name,
                                 "department_id":i.department_id.department_name,
                                 "comment":i.comment
                                 })
        #返回json串
        return json(data)

    def opersgroupproject(self):
        """
        返回给jquery easyui的datagride

        所有运营组项目的数据
        """
        pageIndex= int(request.POST.get('page',0))
        pageSize = int(request.POST.get('rows',20))

        if pageIndex == 0:
            pageIndex = 1
            content = 0
        else:
            content = pageSize * (pageIndex - 1)

        data = {}
        data['total'] = self.opersgroupprojectmodel.all().count()
        data['rows'] = []


        data_info = {'data':self.opersgroupprojectmodel.all().order_by(self.opersgroupprojectmodel.c.id).limit(pageSize).offset(content)}

        for i in data_info['data']:

            #项目负责人
            if i.ogproject_user == '' or i.ogproject_user == None:
                ogproject_user = ''
            else:
                ogproject_user = i.ogproject_user.nickname
                
            #运维负责人
            if i.ogproject_opuser == '' or i.ogproject_opuser == None:
                ogproject_opuser = ''
            else:
                ogproject_opuser = i.ogproject_opuser.nickname                
                
            data['rows'].append({"id":i.id,
                                 "ogproject_name":i.ogproject_name,
                                 "opersgroup_id":i.opersgroup_id.opersgroup_name,
                                 "department_id":i.opersgroup_id.department_id.department_name,
                                 "ogproject_user":ogproject_user,
                                 "ogproject_opuser":ogproject_opuser,
                                 "comment":i.comment
                                 })
        #返回json串
        return json(data)

    def applicationtype(self):
        """
        返回给jquery easyui的datagride

        所有应用主题的数据
        """
        pageIndex= int(request.POST.get('page',0))
        pageSize = int(request.POST.get('rows',20))

        if pageIndex == 0:
            pageIndex = 1
            content = 0
        else:
            content = pageSize * (pageIndex - 1)

        data = {}
        data['total'] = self.applicationtypemodel.all().count()
        data['rows'] = []


        data_info = {'data':self.applicationtypemodel.all().order_by(self.applicationtypemodel.c.id).limit(pageSize).offset(content)}

        for i in data_info['data']:

            data['rows'].append({"id":i.id,
                                 "department_name":i.opersgroupproject_id.opersgroup_id.department_id.department_name,
                                 "opersgroup_name":i.opersgroupproject_id.opersgroup_id.opersgroup_name,
                                 "ogproject_name":i.opersgroupproject_id.ogproject_name,
                                 "applicationtype_name":i.applicationtype_name,
                                 "applicationtype_dep_env":i.applicationtype_dep_env,
                                 "applicationtype_dep_doc":i.applicationtype_dep_doc,
                                 "comment":i.comment
                                 })
        #返回json串
        return json(data)

####################################################################################################    

    def departmentinfo(self):
        """
        获取部门名称，提供给combobox
        """

        data = []
        data_info = {'data':self.departmentmodel.all().order_by(self.departmentmodel.c.id)}

        for i in data_info['data']:

            data.append({"id":i.id,
                         "department_name":i.department_name
                         })

            #返回json串
        return json(data)

    def opersgroupinfo(self):
        """
        获取运营组名称，提供给combobox
        """
        #得到部门id
        _id = request.GET.get('id')

        data = []
        if _id == None or _id == "":
            return json(data)  

        data_info = {'data':self.opersgroupmodel.filter(self.opersgroupmodel.c.department_id == _id)}

        for i in data_info['data']:

            data.append({"id":i.id,
                         "opersgroup_name":i.opersgroup_name
                         })

            #返回json串
        return json(data)     

    def opersgroupprojectinfo(self):
        """
        获取运营组项目名称，提供给combobox
        """
        #得到运营组id
        _id = request.GET.get('id')

        data = []
        if _id == None or _id == "":
            return json(data) 

        data_info = {'data':self.opersgroupprojectmodel.filter(self.opersgroupprojectmodel.c.opersgroup_id == _id)
                     }

        for i in data_info['data']:

            data.append({"id":i.id,
                         "ogproject_name":i.ogproject_name
                         })

            #返回json串
        return json(data)

    def applicationtypeinfo(self):
        """
        获取应用主题名称，提供给combobox
        """
        #得到运营组项目id
        _id = request.GET.get('id')

        data = []
        if _id == None or _id == "":
            return json(data)

        data_info = {'data':self.applicationtypemodel.filter(
            self.applicationtypemodel.c.opersgroupproject_id == _id)
                     }

        for i in data_info['data']:

            data.append({"id":i.id,
                         "applicationtype_name":i.applicationtype_name
                         })

        #返回json串
        return json(data)
    
    def applicationtree(self):
        """
        异步加载
        """
        pid = request.GET.get("pid")
        level = request.GET.get("level")
        treedata = []
        if pid == "0":
                treedata.append({"id":"99999999",
                                 "text":"应用主题展示",
                                 "state":"closed",
                                 "checked":False,
                                 "level":"r"
                                 })
        
        else:
            if level == "r":
                data = self.departmentmodel.all().order_by(self.departmentmodel.c.id)
    
                for i in data:
                    
                    count = self.opersgroupmodel.filter(self.opersgroupmodel.c.department_id == i.id).count()
    
                    if count == 0:
                        state = "open"
                    else:
                        state = "closed"
                    treedata.append({"id":i.id,
                                    "text":i.department_name,
                                    "state":state,
                                    "checked":False,
                                    "level":"dp"
                                    })
            elif level == "dp":
                data = self.opersgroupmodel.filter(self.opersgroupmodel.c.department_id == pid)
    
                for i in data:
                    count = self.opersgroupprojectmodel.filter(
                        self.opersgroupprojectmodel.c.opersgroup_id == i.id).count()
                        
                    if count == 0:
                        state = "open"
                    else:
                        state = "closed"    
                    treedata.append({"id":i.id,
                                    "text":i.opersgroup_name,
                                    "state":state,
                                    "checked":False,
                                    "level":"og"
                                    })            
            elif level == "og":
                data = self.opersgroupprojectmodel.filter(self.opersgroupprojectmodel.c.opersgroup_id == pid)
    
                for i in data:
                    count = self.applicationtypemodel.filter(
                        self.applicationtypemodel.c.opersgroupproject_id == i.id).count()
                        
                    if count == 0:
                        state = "open"
                    else:
                        state = "closed"
                        
                    treedata.append({"id":i.id,
                                    "text":i.ogproject_name,
                                    "state":state,
                                    "checked":False,
                                    "level":"ogp"
                                    })   
            elif level == "ogp":
                data = self.applicationtypemodel.filter(self.applicationtypemodel.c.opersgroupproject_id == pid)
    
                for i in data:
    
                    treedata.append({"id":i.id,
                                    "text":i.applicationtype_name,
                                    "state":"open",
                                    "checked":False,
                                    "level":"ap"
                                    })
            else:
                return json({})
                    
        return json(treedata)    