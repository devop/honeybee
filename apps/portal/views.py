#coding=utf-8
from uliweb import expose,json

from uliweb import request, error, redirect
from uliweb import decorators

@expose('/portal')
class Portal(object):
    
    def __begin__(self):
        """
        用户验证
        """
        from uliweb import functions
        return functions.require_login()

    def index(self):
        return {}

    def notice(self):
        """
        公告
        """
        return {}

    def readme(self):
        """
        说明
        """
        return {}
    
    def about(self):
        """
        关于
        """
        return {}



