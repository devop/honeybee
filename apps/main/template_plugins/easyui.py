#coding=utf-8


#def call(app, var, env, ajaxForm=False, hoverIntent=False, spin=False):
	#a = []
	#a.append('css/bootstrap.min.css')
	#a.append('css/jquery-ui-1.10.0.custom.css')
	
	#if spin:
		#a.append('jqutils/spin.min.js')
	#a.append('jqutils/jqrselect.js')
	#a.append('jqutils/jqutils.js')
	#if ajaxForm:
		#a.append('jqutils/jquery.form.js')
	#if hoverIntent:
		#a.append('jqutils/jquery.hoverIntent.minified.js')
	#return {'toplinks':a, 'depends':[('jquery', {'ui':True})]}
	
def call(app, var, env,theme="default",e_version='1.3.5',j_version='1.11.0'):
	a = []

	easyui_ver = 'jquery-easyui-' + e_version
	jquery_ver = 'jquery-' + j_version + '.min.js'
	#加载easyui主题
	a.append('jquery-easyui-theme/'+theme + '/easyui.css')
	a.append('jquery-easyui-theme/icon.css')
	#扩展图标
	a.append('icons/icon-all.css')
	#加载ext图标
	a.append('icons/icon-ext.css')
	
	#加载jquer和easyui的js
	a.append('jquery/' + jquery_ver)
	a.append(easyui_ver + '/jquery.easyui.min.js')
	a.append(easyui_ver + '/locale/easyui-lang-zh_CN.js')
	
	#加载jquery插件
	a.append('jquery/plugins/jquery.cookie.js')
	
	#easyui插件
	a.append('jeasyui-extensions/jquery-easyui-portal/jquery.portal.js')
	a.append('jeasyui-extensions/jquery-easyui-portal/portal.css')
	
	#easyui扩展验证
	a.append('jeasyui-extensions/jeasyui.extensions.validatebox.js')
	
	return {'toplinks':a}