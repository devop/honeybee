#coding=utf-8


#def call(app, var, env, ajaxForm=False, hoverIntent=False, spin=False):
	#a = []
	#a.append('css/bootstrap.min.css')
	#a.append('css/jquery-ui-1.10.0.custom.css')
	
	#if spin:
		#a.append('jqutils/spin.min.js')
	#a.append('jqutils/jqrselect.js')
	#a.append('jqutils/jqutils.js')
	#if ajaxForm:
		#a.append('jqutils/jquery.form.js')
	#if hoverIntent:
		#a.append('jqutils/jquery.hoverIntent.minified.js')
	#return {'toplinks':a, 'depends':[('jquery', {'ui':True})]}
	
def call(app, var, env,datagrid_filter=False,datagrid_detailview=False):
	a = []
	
	#加载表格过滤
	if datagrid_filter:
		a.append('jeasyui-extensions/datagrid-filter.js')
		
	if datagrid_detailview:
		#datagrid明细扩展
		a.append('jeasyui-extensions/datagrid-detailview.js')

	return {'toplinks':a}