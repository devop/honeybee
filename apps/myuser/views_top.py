#coding=utf-8
from uliweb import expose,json

#可以省略直接引用
from uliweb import request, error, redirect

from models import encrypt_password,check_password

def __begin__():
    """
    用户验证 权限验证
    """
    from uliweb import functions
    return functions.require_login()


@expose('/user/my')
class UserTop(object):

    def __init__(self):
        from uliweb.orm import get_model
        self.usermodel = get_model('user')
    
    
    def userinfo(self):
        """
        top面板用户信息
        """
        userid = request.user
        userip = request.host
        
        userinfo = list(self.usermodel.filter(self.usermodel.c.id == userid).limit(1))[0]

        return {"userinfo":userinfo,"userip":userip}
        
    
    def usermodpwd(self):
        """
        top面板用户信息----密码修改
        """
        
        _id = request.POST.get('id')
        userpasswd = request.POST.get('userpwd')

        user = functions.get_object(self.usermodel, _id, cache=True, use_local=True)
        
        user.password = encrypt_password(userpasswd.strip())
        
        try:
            user.save()
            return json({"success":200,"msg":"密码修改成功"})
        except:
            return json({"error":500,"msg":"密码修改失败"})
    