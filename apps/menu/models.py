#coding=utf-8
from uliweb.orm import *
import datetime
from uliweb import functions

class Menu(Model):
    """
    菜单
    """
    menuname = Field(str, verbose_name='菜单名', max_length=30, unique=True, index=True, nullable=False)
    menuurl = Field(str, verbose_name='url地址', max_length=150)
    pid = Field(int, verbose_name='父ID')
    menuicon = Field(str, verbose_name='菜单图标', max_length=50)
    menuseq = Field(int, verbose_name='菜单排序')
    menupath = Field(str, verbose_name='菜单级别序号', max_length=100)
