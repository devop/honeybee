#coding=utf-8
from uliweb import expose

#可以省略直接引用
from uliweb import request, error, redirect
from uliweb.form import *
from sqlalchemy import distinct

def __begin__():
    """
    用户验证
    """
    from uliweb import functions
    return functions.require_login()


@expose('/menupost')
class PostData(object):

    def __init__(self):
        from uliweb.orm import get_model
        self.menumodel = get_model('menu')
    
    def menu(self):
        """
        返回给jquery easyui的datagride
        
        所有菜单的数据
        """
        menudata = {}
        menudata['total'] = self.menumodel.all().count()
        menudata['rows'] = []

        menu = {'data':self.menumodel.all().order_by(self.menumodel.c.menuseq)}
        
        for i in menu['data']: 
            if int(i.pid) == 0:
                menudata['rows'].append({"id":i.id,
                                 "menuname":i.menuname,
                                 "menuurl":"",
                                 "iconCls":i.menuicon,
                                 "menuseq":i.menuseq,
                                 })
            else:
                pname = list(self.menumodel.filter(self.menumodel.c.id == i.pid))[0]
                
                count_data = self.menumodel.filter(self.menumodel.c.pid == i.id).count()
        
                if int(count_data) == 0:
                    state = "open"
                else:
                    state = "closed"
                
                menudata['rows'].append({"id":i.id,
                                 "menuname":i.menuname,
                                 "menuurl":i.menuurl,
                                 "iconCls":i.menuicon,
                                 "menuseq":i.menuseq,
                                 "pid":i.pid,
                                 "pname":pname.menuname,
                                 "state":state,
                                 "_parentId":i.pid
                                 
                                 })
           
        #返回json串
        return json(menudata)   
    
    def list_to_tree(self,data):
        
        root = list(self.menumodel.filter(self.menumodel.c.pid==0).limit(1))[0]
        
        out = { 
            root.id: { 'id': root.id, 
                       'parent_id': 0, 
                       'text': root.menuname, 
                       "state":"open",
                       "checked":False,
                       "attributes":{"url":root.menuurl},
                       "iconCls":root.menuicon,
                       'children': [] 
                       }
        }
    
        for p in data:
            out.setdefault(p['parent_id'], { 'children': [] })
            out.setdefault(p['id'], { 'children': [] })
            out[p['id']].update(p)
            out[p['parent_id']]['children'].append(out[p['id']])
    
        return out[root.id]

    def menuinfo(self):
        """
        用于combox选择

        """

        treedata = []
        orm_data = self.menumodel.filter(self.menumodel.c.pid != 0).order_by(self.menumodel.c.menuseq)
        
        for i in orm_data:

            count_data = self.menumodel.filter(self.menumodel.c.pid == i.id).count()
    
            if int(count_data) == 0:
                state = "open"
            else:
                state = "closed"
                        
            treedata.append({"id":i.id,
                        "text":i.menuname,
                        "state":state,
                        "checked":False,
                        "attributes":{"url":i.menuurl},
                        "iconCls":i.menuicon,
                        'parent_id':i.pid
                        })
        tree = self.list_to_tree(treedata)
        
        return json([tree])
    
    def menuroleinfo(self):
        """
        用于授权的菜单数,树默认全部展开

        """

        treedata = []
        orm_data = self.menumodel.filter(self.menumodel.c.pid != 0).order_by(self.menumodel.c.menuseq)
        
        for i in orm_data:

            count_data = self.menumodel.filter(self.menumodel.c.pid == i.id).count()
                        
            treedata.append({"id":i.id,
                        "text":i.menuname,
                        "attributes":{"url":i.menuurl},
                        "iconCls":i.menuicon,
                        'parent_id':i.pid
                        })
        tree = self.list_to_tree(treedata)
        
        return json([tree])