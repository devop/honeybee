#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os

CONFIG={}

#上传目录路径
CONFIG['UPLOADS'] = os.path.join(os.path.abspath(os.path.dirname(__file__)),'..','uploads')

#允许上传的文件扩展名
CONFIG['FILE_EXT'] = ('jpg', 'jpeg', 'bmp', 'gif', 'png',"rar" ,"doc" ,"docx","zip","pdf","txt","swf","wmv")
